default: init install

nix:
	curl https://nixos.org/nix/install | sh

init:
	git submodule update --init --recursive
	cd contracts && openzeppelin init

install:
	cd commons && npm install
	cd contracts && yarn

ocean:
	nix-shell --command "barge/start_ocean.sh --no-commons --local-spree-node --purge"
	#--local-spree-no-deploy
	#--local-spree-node

commons-ui:
	cd commons && scripts/keeper.sh
	cd commons && npm start

run-contracts:
	cd ./contracts && nix-shell --command "yarn test"
	cd ./contracts && nix-shell --command "yarn migrate:reset --network development"

run-integration:
	cd ./integration && nix-shell --command "make run"