with import <nixpkgs> { };

stdenv.mkDerivation {
  name = "dev";
  buildInputs = [
    docker
    docker_compose
    nodejs-12_x
    yarn
    pkgconfig
    autoconf
    automake
    libudev
    libtool
    libusb
    libusb.dev
    libusb1
    libusb1.dev
    nasm
    autogen
    zlib
    python
    pythonPackages.pip
    pythonPackages.setuptools
  ];
}
