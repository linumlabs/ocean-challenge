# Molecule Dataswap - Ocean Protocol Data Economy Challenge submission

## Inspiration

Our team is releasing an application called Molecule Catalyst to mainnet the day after the submission deadline. Molecule Catalyst is a reward-based crowdfunding platform to incentivize the creation of novel scientific biotech and pharmaceutical research data and IP. Our inspiration was to create an integration between the Ocean Protocol and Molecule Catalyst, to enable DAO like management of research data produced from the projects funded via Molecule Catalyst, as well as monetizing the research data to improve the value of the Molecule Catalyst markets.

For example, for our first project on Catalyst, the University of Toronto Psychedelic Studies Research Program is running a large scale human study on micro-dosing psilocybin (see picture of study in image gallery). During the study the lab will administer pharmaceutical-grade psilocybin in a lab environment. This study will produce preliminary insights and a data set containing the actual results from patient experiments. These data-sets could then inform further academic study. All data and materials will be made available to the public - and this is a real running use case today, that would benefit from Ocean’s system.

With Molecule Dataswap:
1. Researchers create campaigns to receive funding for their research milestones. 
2. Contributors are rewarded for their support through a token distribution. 
3. At each research phase researchers are required to publish updates and data to prove their accomplishments and secure funding for that phase.
4. These datasets will then be published on the Ocean infrastructure where interested parties can buy them.
5. All proceedings from the sales are returned to the Token Bonding Curve to share the revenues with all supporters.

We realize that in these markets, there is a lot of value in the research data being produced, and to incentivize further funding of the project the contributors need to be directly invested in the production of good research data. With Molecule Dataswap, built on the Ocean Protocol, we can achieve that.

Currently the Ocean Protocol has a static pricing system, and if data is the new oil, this needs to change. We believe we can greatly improve and build upon the Ocean Protocol feature set by introducing dynamic pricing and distributed ownership of datasets produced from the Molecule application.

## What it does

Molecule Dataswap is a set of smart contracts and an oracle that allows data producers to fund the publication of a dataset, and offer access to the dataset at a dynamic price according to the state of the dataset bonded market. The dataset is essentially entered into and managed by a DAO, that fairly distributes Ocean token rewards to contributors in the form of dividends, and increases existing token holders’ token value.

## How we built it

**Note:** we couldn't use the Nile/Pacific networks for this challenge, as we were using some of the updated APIs and features such as service agreements, which seemed to have issues with the publicly deployed infrastructure. Instead we used Barge with a few adjustments to service versions - see https://github.com/oceanprotocol/squid-py/issues/421

### Dynamic Pricing

Currently, the Ocean Protocol has static asset pricing. There are API endpoints for updating the asset metadata price via the Aquarius API. When introducing dynamic pricing, in our use case, the asset should be priced according to:

1. The funding status of the project generating the data
2. The popularity of the dataset

This is achieved by collecting the Ocean token rewards in a bonding curve collateral pool. A bonding curve is a function that defines the relationship between the token supply, underlying collateral (Ocean tokens), and the token price. 

We used the token price to set the price of Ocean assets. This means, the more contributors a research project has, the higher the value of the token. The more popular the Ocean dataset is, and the more it is consumed, the more the initial contributors benefit by being token holders.

### Smart Contracts

We forked the BC-DAO "Bonding Curves For DAOs" project as a good base for the bonding curve interface. We specifically needed to use an invariant preserving bonding curve implementation so that the function depends on the collateral deposited. The contracts were refactored to allow depositing Ocean ERC20 tokens without directly calling the bonded market functions like "buy" and "sell". This allowed us to set the bonded market as the Ocean Protocol beneficiary for asset consumption. Every time an asset is consumed, the Ocean reward is sent to the bonded market, which increases the underlying collateral - this dynamically adjusts the bonded token price.

### Oracle

When the bonded token price changes, the oracle updates the Ocean token price of the relevant asset via the Aquarias API and the asset DDO metadata.

### UI/UX

We designed our UI in Figma first, knowing that this was predominantly a contract and backend hack. You'll see we implemented certain parts of the frontend and left others.

The consumer needs to be aware of the price they are paying for a particular asset, as well as the price history. The price history gives the consumer an indication of the popularity of the asset, and ideally quality too. The higher quality assets should receive more funding and consumption, both of which contribute to the asset price.

Although not implemented in this hack, a basic price history graph was designed to communicate the above.

When a publisher creates an asset for consumption, they are given the option of which bonded market address they'd like to set as the beneficiary. The beneficiary is set and the DID ownership is transferred.

## Challenges we ran into

We ended up choosing a hack that delved quite deep into the Ocean stack, tested the limits of our understanding of the Ocean stack, as well as the capabilities of the Ocean stack.

During the course of the Ocean challenge we opened 3 merge requests, and I think that's part of the value of a hackathon, learning and contributing back to the supporting project. Two of the MRs were merged, and the third MR is in progress.

1. https://github.com/oceanprotocol/react-tutorial/issues/2
2. https://github.com/oceanprotocol/aquarius/pull/226
3. https://github.com/oceanprotocol/squid-py/issues/421

The Ocean ecosystem is composed of multiple micro-services, and most of the issues we ran into were because of versioning and out of date documentation. It was a learning experience, and we'd feel much more confident hacking with the Ocean Protocol in future.

There were also challenges related to asset ownership and permissions being transferred. In order to set the beneficiary, at least via the Squid API, the ownership and associated permissions are also transferred. This makes admin tasks like granting service agreement access difficult, and was a blocker for us wiring everything up. We realize there is probably a way to address this at a lower level, but not with the current API interfaces. 

## Accomplishments that we're proud of

We touched on almost all parts of the Ocean stack, made our first contributions to the various Ocean Protocol repos, and became familiar with what will most likely be an important feature in the Molecule application. This hack has a real world use case today, and once ready could easily be integrated into the Molecule stack.

## What we learned

* The low level functionality of the Ocean Protocol
* The intricacies of the Bancor & BC-DAO bonding curve implementation
* What an integration between Ocean and Molecule could look like

## What's next for Molecule Dataswap

In future we'll look into updating the contracts to be multi-collateral - this would mean Ocean would contribute value for dataset consumption, and perhaps other protocols could slot in elsewhere.

With further help from the Ocean team and more time, the different parts of our hack will surely find their way into the Molecule stack, and provide a useful feature of dynamic pricing on top of the Ocean Protocol.

## User Flow

![User flow](./media/ux.png)

## System Architecture

![System architecture](./media/system.png)

# Development

You'll need Make, Nix, and OpenZeppelin installed to run this project.

## Install Nix

1. `make nix`

## Initialize project

1. `nix-shell shell.nix`
2. `make init`

## Setup Environment

1. `make ocean` - sets up Barge
2. `make commons-ui` - starts Commons UI fork

## Tests

1. `make run-contracts` - test contracts and deploy contracts
2. `make run-integration` - run Python squid-py integration test (WIP)

### Contract Tests & Migration

Go to `http://localhost:3000/about`, Brizo, to get the `OceanToken` address. This needs to be set in the `contracts/migrations/config.js` file as `collateralToken`.

1. `cd ./contracts`
2. `yarn test`
3. `yarn migrate:reset --network development`

The contract addresses will be logged, you'll need to use the bonding curve address as the receiver of the Ocean token rewards, used in the `transferOwnership` process described below.

## Resources

Aquarius API endpoint: http://0.0.0.0:5000/api/v1/docs/

See `integration/aquarius.py` for WIP example of updating asset DDO metadata. See issue blocker and PR https://github.com/oceanprotocol/aquarius/pull/225

See `commons/client/src/routes/Publish/index.tsx` line 315 for `transferOwnership` process.

## WIP Issues

1. https://github.com/dOrgTech/BC-DAO/issues/61 - in order to calculate `priceToBuy` etc. in the bonding curve contracts, it is required that the total supply of the bonded token is non zero. This is causing an issue at the moment, which should be sorted by minting an initial token supply as seen in the migrations, but for some reason doesn't seem to be working.
