with import <nixpkgs> { };

stdenv.mkDerivation {
  name = "python";
  buildInputs = [
    python36
    python36Packages.pip
    python36Packages.setuptools
    python36Packages.virtualenvwrapper
    pandoc
    gcc
    libffi
  ];
  shellHook = ''
    export SOURCE_DATE_EPOCH=315532800
    [ ! -d "./venv" ] && virtualenv venv -p python3.6
    source venv/bin/activate
  '';
}
