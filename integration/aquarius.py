#!/usr/bin/env nix-shell
#!nix-shell -i python -p python36 python36Packages.requests

import requests

url = "http://0.0.0.0:5000/api/v1/aquarius/assets"

response = requests.request("GET", url)

print(response.text)

ddo = response.json()["ids"][-1]
url = f"http://0.0.0.0:5000/api/v1/aquarius/assets/ddo/{ddo}"

response = requests.request("GET", url)

print(response.text)

payload = response.text
headers = {"Accept": "application/json", "Content-Type": "application/json"}

response = requests.request("PUT", url, data=payload, headers=headers)

print(response.text)
