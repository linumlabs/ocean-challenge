import os
import time

from web3 import Web3

from ocean_utils.ddo.metadata import Metadata
from ocean_utils.agreements.service_agreement import ServiceAgreement
from ocean_utils.agreements.service_types import ServiceTypes

from ocean_keeper.account import Account
from ocean_keeper.utils import get_account
from ocean_keeper.didregistry import DIDRegistry
from ocean_keeper.web3_provider import Web3Provider
from ocean_keeper.diagnostics import Diagnostics

from squid_py import Ocean, ConfigProvider, Config
from squid_py.ocean.keeper import SquidKeeper as Keeper

from examples import example_metadata

def setup_config():
    # Prepare environment account variables
    os.environ["PARITY_ADDRESS"] = "0x00bd138abd70e2f00903268f3db08f2d25677c9e"
    os.environ["PARITY_PASSWORD"] = "node0"
    os.environ["PARITY_ADDRESS1"] = "0x068ed00cf0441e4829d9784fcbe7b9e26d4bd8d0"
    os.environ["PARITY_PASSWORD1"] = "secret"
    # Specify path to the private keys
    os.environ["PARITY_KEYFILE"] = "./keyfiles/key_file_2.json"
    os.environ["PARITY_KEYFILE1"] = "./keyfiles/key_file_1.json"

    ConfigProvider.set_config(Config("config.ini"))

def transfer_ownership(asset_id, new_owner, account):
    did_registry = DIDRegistry.get_instance()
    assert did_registry.get_did_owner(asset_id) != new_owner, "Invalid new owner"
    did_registry.transfer_did_ownership(asset_id, new_owner, account)
    assert did_registry.get_did_owner(asset_id) == new_owner, "Ownership transfer failed"

w3 = Web3

# Make a new instance of Ocean
setup_config()
ocean = Ocean()
config = ocean.config

#It is also possible to initialize account as follows bypassing the creation of environment variables
#account = Account(Web3.toChecksumAddress(address), pswrd, key_file, encr_key, key)
publisher = get_account(0) # use if env vars are declared
consumer_account = get_account(1) # PARITY_ADDRESS1 PARITY_KEY1 & PARITY_PASSWORD1
delegate_account = Account(Web3.toChecksumAddress("0x00bd138abd70e2f00903268f3db08f2d25677c9e"), "node0", "./keyfiles/publisher_key_file.json")

# Diagnostics.verify_contracts()
keeper = Keeper.get_instance()

# PUBLISHER
# Let's start by registering an asset in the Ocean network
# metadata = Metadata.get_example()
metadata = example_metadata.metadata

# consume and service endpoints require `brizo.url` is set in the config file
# or passed to Ocean instance in the config_dict.
# define the services to include in the new asset DDO

ddo = ocean.assets.create(metadata, publisher, providers=[])
assert ddo is not None, f'Registering asset on-chain failed.'
print("Asset created")

assert not ocean.assets.get_permissions(ddo.did, delegate_account.address)
ocean.assets.delegate_persmission(ddo.did, delegate_account.address, publisher)
assert ocean.assets.get_permissions(ddo.did, delegate_account.address)

market_address = "0x6E02Fa9A3b60d794787a229B0325F460cd080AF1"
transfer_ownership(ddo.asset_id, market_address, delegate_account)
print("Asset ownership transferred to bonded market")

# Now we have an asset registered, we can verify it exists by resolving the did
_ddo = ocean.assets.resolve(ddo.did)
# ddo and _ddo should be identical
print(_ddo.did)

# CONSUMER
# search for assets
asset_ddo = ocean.assets.search("Ocean protocol")[0]
# Need some ocean tokens to be able to order assets
ocean.accounts.request_tokens(publisher, 10)
print("Consumer request tokens success")

service = ddo.get_service(service_type=ServiceTypes.ASSET_ACCESS)

service_agreement_id = ocean.assets.order(ddo.did, service.index, consumer_account, auto_consume=True)

event_wait_time = 10
event = ocean.keeper.agreement_manager.subscribe_agreement_created(
    service_agreement_id,
    event_wait_time,
    None,
    (),
    wait=True
)
assert event, 'no event for EscrowAccessSecretStoreTemplate.AgreementCreated'
print("Agreement created")

#  check if the lock reward goes through
event = ocean.keeper.lock_reward_condition.subscribe_condition_fulfilled(
    service_agreement_id,
    120,
    None,
    (),
    wait=True
)
assert event, 'no event for LockRewardCondition.Fulfilled'
print("Lockreward success")

print("Grant access to consumer failed!")

# ocean.agreements.conditions.grant_access(
#     service_agreement_id, ddo.did, consumer_account.address, delegate_account)
# event = ocean.keeper.access_secret_store_condition.subscribe_condition_fulfilled(
#     service_agreement_id,
#     120,
#     None,
#     (),
#     wait=True
# )
# assert event, 'no event for AccessSecretStoreCondition.Fulfilled'
# assert ocean.agreements.is_access_granted(service_agreement_id, ddo.did, consumer_account.address)
# print("Grant access to consumer success")

# assert ocean.assets.consume(service_agreement_id, ddo.did, service.index, consumer_account, config.downloads_path)
# print("Asset consumed")

# # after a short wait (seconds to minutes) the asset data files should be available in the `downloads.path` defined in config
# # wait a bit to let things happen
# time.sleep(20)

# # Asset files are saved in a folder named after the asset id
# dataset_dir = os.path.join(
#     ocean.config.downloads_path, f"datafile.{asset_ddo.asset_id}.0"
# )
# if os.path.exists(dataset_dir):
#     print("asset files downloaded: {}".format(os.listdir(dataset_dir)))
